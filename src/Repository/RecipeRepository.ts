import { Connexion } from './../Database/Connexion';
import { Recipe } from '../Entity/Recipe';
import { Connection, createConnection } from "mysql";

import { promisify } from "util";




export class RecipeRepository {
    private query;
    constructor() {
        let databaseConnexion = new Connexion();
        this.query = databaseConnexion.getQuery();
    }


    async findAllRecipe(): Promise<Recipe[]> {
        let result = await this.query("SELECT * FROM Recipe");
        return result.map(row => new Recipe(row['name'], row['category'], row['picture'], row['score']));


    }

    async addRecipe(recipe: Recipe): Promise<number> {
        let result = await this.query('INSERT INTO person (name,age) VALUES (?,?)', [
            recipe.getName(),
            recipe.getCategory(),
            recipe.getPicture(),
            recipe.getScore()
        ]);
        let id = result.insertId;
        recipe.setId(id);
        return id;
    }
    async CreateRecipe(recipe: Recipe): Promise<string> {
        let result = await this.query('INSERT INTO Recipe (name, category, picture, score, ingredient) VALUES (?, ?, ?, ?, ?)', [
            recipe.getName()
        ]);
        let id = result.insertId;
        recipe.setId(id);
        return id;
    }

    async updateRecipe(recipe: Recipe) {

        let changeRecipe = await this.query('UPDATE Recipe SET ingredient = "ingredient" WHERE !ingredient)')

        return changeRecipe;
    }

    async deleteRecipe(recipe: Recipe): Promise<Recipe[]> {

        let deleteRecipe = await this.query('DELETE FROM Recipe WHERE id != id)');

        return deleteRecipe;

    }
}
