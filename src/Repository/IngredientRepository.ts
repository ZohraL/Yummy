import { Connexion } from './../Database/Connexion';
import { Connection, createConnection } from "mysql";

import { promisify } from "util";
import { Ingredient } from "../Entity/Ingredient";



export class IngredientRepository {
    private query;
    constructor() {
        let databaseConnexion = new Connexion();
        this.query = databaseConnexion.getQuery();
    }
    async findAllIngredient(): Promise<Ingredient[]> {
        let result = await this.query("SELECT * FROM Ingredient");
        //On peut faire la conversion en entité avec un map
        return result.map(row => new Ingredient(row['name'], row['id']));


        //Ou bien avec une boucle
        /*
        let persons = [];
        for(const row  of result) {
            persons.push(new Person(row['name'], row['age'], row['id']));
        }
        return persons;
        */
    }
    async addIngredient(ingredient: Ingredient): Promise<string> {
        let result = await this.query('INSERT INTO Ingredient (name) VALUES (?)', [
            ingredient.getName()
        ]);
        let id = result.insertId;
        ingredient.setId(id);
        return id;
    }


    async deleteIngredient(ingredient: Ingredient): Promise<Ingredient[]> {

        let deleteIngredient = await this.query('DELETE FROM Ingredient WHERE id != id)');

        return deleteIngredient;

    }
}